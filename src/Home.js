import React from 'react';
import Employees from './Employees';
import Update from './UpdateEmployee';
import Delete from './DeleteEmployee';
import Insert from './InsertEmployee';
import { BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom";

/* class NotFound extends React.Component {
    render(){return 'Not Found'}
} */

class Parameter extends React.Component {
    render(){
        let query = new URLSearchParams(this.props.location.search);
        let name = query.get('name')
        console.log(this.props.history)
        return (
            <div>
                {name ? (
                    <h3>
                    The <code>name</code> in the query string is &quot;{name}
                    &quot;
                    </h3>
                ) : (
                    <h3>There is no name in the query string</h3>
                )}
            </div>
        )
    }
}

class Home extends React.Component {
    render() {
        return (
            <Router>
                <div>
                    <Switch>
                        {/* Redirect khi uri='/employees' đến '/' */}
                        <Redirect strict from='/employees' to='/' />
                        <Route path="/" exact><Employees /></Route>
                        <Route path="/create"><Insert /></Route>
                        <Route path="/update/:id" component={Update} />
                        <Route path="/delete/:id" component={Delete} />
                        <Route path="/params" component={Parameter}/>
                        <Route path='*' render={() => <div>Not Found</div>}/>
                    </Switch>
                </div>
            </Router>
        )
    }
}

export default Home