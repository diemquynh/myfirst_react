import React from 'react';

class MyForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = { username: '', age: null, errorMessage: '', comment: '', mycar: 'Ford', gender: 'Nam', job: []
            /* teacher:false, student:false, developer:false */};
    }
    myChangeHandler = (event) => {
        let name = event.target.name
        let value = event.target.value
        let err = ''
        if(name === 'age'){
            if(value !== '' && !Number(value)){
                err = <strong>Your age must be a number</strong>
            }
        }
        this.setState({errorMessage: err})    
        this.setState({[name]: value});
    }
    changeStatus = (arg) => {
        let job = this.state.job
        let index = this.state.job.indexOf(arg)
        if(index >= 0) job.splice(index,1)
        else job.push(arg)
        this.setState({job: job})
    }
    // changeTeacher = () => {this.setState(init => ({teacher: !init.teacher}))}
    // changeStudent = () => {this.setState(init => ({student: !init.student}))}
    // changeDeveloper = () => {this.setState(init => ({developer: !init.developer}))}
    mySubmitHandler = (event) => {
        event.preventDefault();
        // let age = this.state.age;
        // if (!Number(age)) {
        //     alert("Your age must be a number");

        // let job = '';
        // if(this.state.teacher) job += 'teacher,'
        // if(this.state.student) job += 'student,'
        // if(this.state.developer) job += 'developer,'
        // alert(job)

        alert(this.state.job.toString())
    }
    render(){
        let header = '';
        if (this.state.username) {
            header = <h1>Hello {this.state.username} {this.state.age} {this.state.comment} {this.state.mycar} {this.state.gender}</h1>;
        }
        const inputText = {backgroundColor:'yellow', margin: '10px 0'} 
        return (
            <form onSubmit={this.mySubmitHandler}>
                {header}
                <p>Enter your name:</p>
                <input type="text" name='username' onChange={this.myChangeHandler} style={inputText}/>
                <input type="text" name='age' onChange={this.myChangeHandler} style={inputText}/>
                {this.state.errorMessage}<br/>
                <textarea name='comment' /* value={this.state.comment} */ onChange={this.myChangeHandler}/>
                <select name='mycar' /* value={this.state.mycar} */ onChange={this.myChangeHandler} style={{color:'red', padding: '5px'}}>
                    <option value="Ford">Ford</option>
                    <option value="Volvo">Volvo</option>
                    <option value="Fiat">Fiat</option>
                </select>
                <div onChange={this.myChangeHandler}>
                    <input type='radio' name='gender' value='Nam'/> Nam
                    <input type='radio' name='gender' value='Nữ' /> Nữ
                    <input type='radio' name='gender' value='Other' /> Other
                </div>
                <div>
                    <input type='checkbox' name='job' onChange={() => this.changeStatus('teacher')} value='teacher' /* checked='true' *//>Teacher
                    <input type='checkbox' name='job' onChange={() => this.changeStatus('student')} value='student'/>Student
                    <input type='checkbox' name='job' onChange={() => this.changeStatus('developer')} value='developer'/>Developer
                </div>
                
                <input type='submit'/>
            </form>
        )
    }
}

export default MyForm