import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Employee from './HookEmployee';
import { Link, NavLink, Prompt } from "react-router-dom";
import './Employees.css';

function Employees () {
    const [employees, setEmployees] = useState([])
    const [currentPage, setCurrentPage] = useState(1)
    const [recordPage, setRecordPage] = useState(10)

    
    // async/await
    async function getData() {
        const res = await axios.get(`http://localhost:8000/employees`)
        console.log(res)
        setEmployees(res.data.data);
    }
    useEffect(() => { getData(); }, []);

    /* useEffect(() => {
        axios.get(`http://localhost:8000/employees`).then(res => {
            console.log(res)
            setEmployees(res.data.data);
        }).catch(err => console.log(err))
    },[]) */

    // Pagination
    function chosePage(event) {
        setCurrentPage(event.target.id);
    }
      
    function selectNumberRecord (event) {
        setRecordPage(event.target.value)
        setCurrentPage(1);
    }

    const indexOfLastNews = currentPage * recordPage;
    const indexOfFirstNews = indexOfLastNews - recordPage;
    const dataArray = employees.slice(indexOfFirstNews,indexOfLastNews)
    const pageNumbers = Math.ceil(employees.length / recordPage);
    const pageArray = []
    for (let i = 1; i <= pageNumbers; i++) {
        pageArray.push(i);
    }
    const data = dataArray.map((employee) => {
        return ( <Employee 
            id = {employee.id}
            name = {employee.name}
            salary = {employee.salary}
            age = {employee.age}
            image = {employee.image}
            key = {employee.id}
        /> )
    })

    return(
        <div>
            {/* Xác nhận xóa employee */}
            <Prompt message={(params) => params.pathname.startsWith('/delete') ? "Delete?" : true } />

            {/* Parameters */}
            <NavLink to = '/params?name=DiemQuynh' target='_blank'>Example about parameter</NavLink>

            <br/> <br/>
            <Link to = '/create'>Insert an employee</Link>
                
            <table className='table'>
                <thead>
                    <tr><th>ID</th><th>Name</th><th>Salary</th><th>Age</th><th>Image</th><th colSpan='2'>Options</th></tr>
                </thead>
                <tbody>
                    { data }
                </tbody>
            </table>
            {/* Pagionation */}
            <div  className="selectNumberRecord">
                <select defaultValue="0" onChange={selectNumberRecord} >
                     <option value="0" disabled>Get by</option>
                    <option value="3">3</option>
                    <option value="5">5</option>
                    <option value="7">7</option>
                    <option value="10">10</option>
                </select>
            </div>
            <div className="pagination-custom">
                 <ul id="page-numbers">
                    {
                        pageArray.map(number => {
                            if (currentPage === number) {
                                return (
                                    <li key={number} id={number} className="active">{number}</li>
                                )
                            }
                            else {
                                return (
                                    <li key={number} id={number} onClick={chosePage} >{number}</li>
                                )
                            }
                        })
                    }
                </ul>
            </div>
        </div>
    );
}

export default Employees