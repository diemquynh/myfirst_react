import React from 'react';
import Employees from './HookEmployees';
import Update from './HookUpdateEmployee';
import Delete from './HookDeleteEmployee';
import Insert from './HookInsertEmployee';
import { BrowserRouter as Router, Route, Switch, Redirect, useLocation } from "react-router-dom";

/* class NotFound extends React.Component {
    render(){return 'Not Found'}
} */

function Parameter () {
    let query = new URLSearchParams(useLocation().search);
    let name = query.get('name')
    return (
        <div>
            {name ? (
                <h3>The <code>name</code> in the query string is &quot;{name}&quot;</h3>
                ) : (
                    <h3>There is no name in the query string</h3>
                )}
        </div>
    )
}

function Home() {
    return (
        <Router>
            <div>
                <Switch>
                    {/* Redirect khi uri='/employees' đến '/' */}
                    <Redirect strict from='/employees' to='/' />
                    <Route path="/" exact><Employees /></Route>
                    <Route path="/create"><Insert /></Route>
                    <Route path="/update/:id" component={Update} />
                    <Route path="/delete/:id" component={Delete} />
                    <Route path="/params"><Parameter /></Route>
                    <Route path='*' render={() => <div>Not Found</div>}/>
                </Switch>
            </div>
        </Router>
    )
}

export default Home