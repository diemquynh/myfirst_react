import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Redirect, useParams/* , useHistory */ } from "react-router-dom";

function Delete () {
    const { id } = useParams()
    const [redirect, setRedirect] = useState( false )
    // const history = useHistory()

    useEffect(() => {
        axios.delete(`http://localhost:8000/delete/${id}`).then((res) => {
            if(res.data === 0) alert(`Not exists employee with id:${id}`)
            // history.goBack()
            setRedirect( true )
        }).catch(err => console.log(err))
    },[id/* , history */])

    if(redirect) 
        return <Redirect to="/" />
    return(<div></div>)
}

export default Delete