import React from 'react';
import axios from 'axios';
// import ReactTable from "react-table-6";
// import 'react-table-6/react-table.css'
import Employee from './Employee';
import { Link, NavLink, Prompt } from "react-router-dom";
import './Employees.css';

class Employees extends React.Component {
    constructor(props) {
        super(props);
        this.state = { employees: [], currentPage:1, recordPage:10 };
    }

    componentDidMount() {
        axios.get(`http://localhost:8000/employees`).then(res => {
            console.log(res)
            const employees = res.data.data;
            this.setState({ employees });
        }).catch(err => console.log(err))
    }

    loadData = () => { console.log(this.state.employees) }

    // Pagination
    chosePage = (event) => {
        this.setState({ currentPage: Number(event.target.id) });
    }
      
    selectNumberRecord = (event) => {
        this.setState({ recordPage: event.target.value, currentPage: 1 })
    }

    render() {
        // Plugin 'react-table-6'
        /* let columns = [{
            Header: 'Infomations',
            columns: [
                {Header: 'ID', accessor: 'id'},
                {Header: 'Name', accessor: 'name'},
                {Header: 'Salary', accessor: 'salary'},
                {Header: 'Age', accessor: 'age'},
                {Header: 'Image', accessor: 'image'}
            ]
        }] */

        // Don't Paginate
        /* const data = this.state.employees.map((employee) => {
            return ( <Employee 
                id = {employee.id}
                name = {employee.name}
                salary = {employee.salary}
                age = {employee.age}
                image = {employee.image}
                key = {employee.id}
            /> )
        }) */
        const currentPage = this.state.currentPage
        const recordPage = this.state.recordPage
        const indexOfLastNews = currentPage * recordPage;
        const indexOfFirstNews = indexOfLastNews - recordPage;
        const dataArray = this.state.employees.slice(indexOfFirstNews,indexOfLastNews)
        const pageNumbers = Math.ceil(this.state.employees.length / recordPage);
        const pageArray = []
        for (let i = 1; i <= pageNumbers; i++) {
            pageArray.push(i);
        }
        const data = dataArray.map((employee) => {
            return ( <Employee 
                id = {employee.id}
                name = {employee.name}
                salary = {employee.salary}
                age = {employee.age}
                image = {employee.image}
                key = {employee.id}
            /> )
        })

        return(
            <div>
                {/* Xác nhận xóa employee */}
                <Prompt message={(params) => params.pathname.startsWith('/delete') ? "Delete?" : true } />
                {/* <ReactTable
                    data={this.state.employees}
                    columns={columns}
                    defaultPageSize={2}
                /> */}

                {/* Parameters */}
                <NavLink to = '/params?name=DiemQuynh' target='_blank'>Example about parameter</NavLink>

                <br/> <br/>
                <Link to = '/create'>Insert an employee</Link>
                
                <table className='table'>
                    <thead>
                        <tr><th>ID</th><th>Name</th><th>Salary</th><th>Age</th><th>Image</th><th colSpan='2'>Options</th></tr>
                    </thead>
                    <tbody>
                        { data }
                    </tbody>
                </table>
                {/* Pagionation */}
                <div  className="selectNumberRecord">
                    <select defaultValue="0" onChange={this.selectNumberRecord} >
                        <option value="0" disabled>Get by</option>
                        <option value="3">3</option>
                        <option value="5">5</option>
                        <option value="7">7</option>
                        <option value="10">10</option>
                    </select>
                </div>
                <div className="pagination-custom">
                    <ul id="page-numbers">
                        {
                            pageArray.map(number => {
                                if (this.state.currentPage === number) {
                                    return (
                                        <li key={number} id={number} className="active">{number}</li>
                                    )
                                }
                                else {
                                    return (
                                        <li key={number} id={number} onClick={this.chosePage} >{number}</li>
                                    )
                                }
                            })
                        }
                    </ul>
                </div>
                {/* <button onClick={this.loadData}>Show data!</button> */}
            </div>
        );
    }
}

export default Employees