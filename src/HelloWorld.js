import React from 'react';

/* function HelloWorld() {
    return (
      <div>Hello World!</div>
    );
} */

class House extends React.Component {
    render(){return (<div>I have a {this.props.color} house</div>)}
}

class Child extends React.Component {
    // UNMOUNTING: componentWillUnmount
    componentWillUnmount() {
      alert("The component named Header is about to be unmounted.");
    }
    render() {
      return (
        <h1>Hello React!</h1>
      );
    }
  }

class HelloWorld extends React.Component {
    // MOUNTING: constructor, getDerivedStateFromProps, render, componentDidMount
    constructor(props) {
        super(props);
        this.state = {color: "red", size:'S', show: true};
        // popular function don't have argument
        this.popular = this.popular.bind(this)
    }
    /* static getDerivedStateFromProps(props, state) {
        return {color: props.model.color };
    } */
    componentDidMount() {
        setTimeout(() => {
          this.setState({color: "blue"})
        }, 5000)
        // document.getElementById('change').addEventListener('click',this.changeColor);
    }
    // UPDATING: getDerivedStateFromProps, shouldComponentUpdate, render, getSnapshotBeforeUpdate, componentDidUpdate
    changeColor = () => {
        this.setState({color: 'yellow'})
    }
    shouldComponentUpdate() {
        return true;
    }
    getSnapshotBeforeUpdate(prevProps, prevState) {
        document.getElementById("div1").innerHTML =
        "Before the update, the T-shirt's color was " + prevState.color;
    }
    componentDidUpdate() {
        document.getElementById("div2").innerHTML =
        "The updated T-shirt's color is " + this.state.color;
    }
    // UNMOUNTING: componentWillUnmount
    delHeader = () => {
        this.setState({show: false});
    }
    // popular function
    popular() {
        alert('this is popular function');
    }
    // popular function have argument
    popularArgument(a,b) {
        alert(a + ' & ' + b);
    }
    popularEvent(a,b,c) {
        alert(a + ', ' + b + ' & ' + c.type);
    }
    // arrow function have argument
    arrowArgument = (a,b) => {
        alert(a + ' & ' + b);
    }
    // Third argument is event
    arrowEvent = (a,b,c) => {
        alert(a + ', ' + b + ' & ' + c.type);
    }
    render(){
        let myheader = '';
        if (this.state.show) {
            myheader = <Child />;
        };
        return (
            <div>
                <h1>Hello World!</h1>
                <div>I have a {this.state.color} T-shirt with {this.state.size} size and a {this.props.model.color} skirt</div>
                <House color='pink'/>
                <div id="div1"></div>
                <div id="div2"></div>
                <button type='button' id='change' onClick={this.changeColor}>Change color</button>
                {myheader}
                <button type="button" onClick={this.delHeader}>Delete Header</button>
                <h3>Function</h3>
                <button onClick={this.popular}>popular function</button>
                <button onClick={this.popularArgument.bind(this,'5','10')}>popular function have argument</button>
                {/* hoặc onClick={() => this.popularArgument('5','10')} */}
                <button onClick={this.popularEvent.bind(this,'5','10')}>show event</button>
                <br/>
                <button onClick={() => this.arrowArgument("Goal",'Silver')}>arrow function have argument!</button>
                {/* hoặc onClick={this.arrowArgument.bind(this,"Goal",'Silver')} */}
                <button onClick={(ev) => this.arrowEvent("Goal",'Silver',ev)}>Show the Event!</button>
            </div>
        );
    }
}
export default HelloWorld