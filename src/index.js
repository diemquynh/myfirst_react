import React from 'react';
import ReactDOM from 'react-dom';
// import HelloWorld from './HelloWorld';
// import MyForm from './MyForm2';
// import './MyForm2.css';
import MyForm from './UncontrollForm';
// import Home from './Home';
// import Home from './HookHome';
import * as serviceWorker from './serviceWorker';

// let model = {size:'S', color:'brown'}
// ReactDOM.render(<HelloWorld model={model}/>, document.getElementById('root'));

ReactDOM.render(<MyForm />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();