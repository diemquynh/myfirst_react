import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Redirect, useParams, useHistory } from "react-router-dom";

function Update () {
    const { id } = useParams()
    const [employee, setEmployee] = useState({ id:id, name: '', salary: '', age: '', image: '' })
    const [redirect, setRedirect] = useState( false )
    const history = useHistory()
    const [error, setError] = useState({ name:'', salary:'', age:'' })

    useEffect(() => {
        axios.get(`http://localhost:8000/employee/${id}`).then(res => {
            const data = res.data.data;
            console.log(res.data)
            if(data){
                setEmployee(prevState => ({ ...prevState, name: data.name, salary: data.salary, age: data.age }));
            }
            else{
                document.getElementById('submit').disabled = true
                alert(`Not exists employee: id = ${id}`)
                history.push('/')
            }
        })
    },[id, history])

    function myChangeHandler (event) {
        let name = event.target.name
        let value = event.target.value
        let err = ''
        if(name === 'age' || name === 'salary'){
            if(value !== '' && !Number(value)){
                err = <strong>Your {name} must be a number</strong>
                setError(prev => ({...prev, [name]: err}))
            }
            else setError(prev => ({...prev, [name]: ''}))
        }
        else if(name === 'name') {
            err = <strong>Fill your {name}</strong>
            value === '' ? setError(prev => ({...prev, [name]: err})) : setError(prev => ({...prev, [name]: ''}))
        }
        setEmployee(prevState => ({ ...prevState, [name]: value}));
    }

    function mySubmitHandler (event) {
        event.preventDefault();
        const {id:ID, ...other} = employee
        console.log({...other})
        if(error.age === '' && error.salary === '' && error.name === '')
        axios.put(`http://localhost:8000/update/${ID}`, {...other}).then(() => {
            // window.history.back()
            // history.goBack()
            setRedirect( true )
        }).catch(err => console.log(err))
    }
    
    return (
        <div>
            {redirect ? <Redirect to='/' /> : '' }
            <h2>Update employees's informations</h2>
            <form id='update' onSubmit={mySubmitHandler}>
                <div>
                    ID:<br/>
                    <input type='text' name='id' value={employee.id} readOnly/>
                </div>
                <div>
                    Fill name:<br/>
                    <input type='text' name='name' value={employee.name} onChange={myChangeHandler}/> {error.name}
                </div>
                <div>
                    Fill salary:<br/>
                    <input type='text' name='salary' value={employee.salary} onChange={myChangeHandler}/> {error.salary}
                </div>
                
                <div>
                    Fill age:<br/>
                    <input type='text' name='age' value={employee.age} onChange={myChangeHandler}/> {error.age}
                </div>
                <input type='submit' id='submit'/>
            </form>
        </div>
    )
}

export default Update