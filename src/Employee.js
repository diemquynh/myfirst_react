import React from 'react';
import { Link } from "react-router-dom";

class Employee extends React.Component {
    render(){
        let locationUpdate = '/update/' + this.props.id
        let locationDelete = '/delete/' + this.props.id
        return(
            <tr>
                <td>{this.props.id}</td>
                <td>{this.props.name}</td>
                <td>{this.props.salary}</td>
                <td>{this.props.age}</td>
                <td>{this.props.image}</td>
                <td><Link to = {locationUpdate}>Update</Link></td>
                <td><Link to = {locationDelete}>Delete</Link></td>
            </tr>
        )
    }
}

export default Employee