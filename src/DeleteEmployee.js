import React from 'react';
import axios from 'axios';
import { Redirect } from "react-router-dom";

class Delete extends React.Component {
    constructor(props){
        super(props)
        this.state = { redirect: false }
    }
    componentDidMount(){
        const id = this.props.match.params.id;
        axios.delete(`http://localhost:8000/delete/${id}`).then((res) => {
            if(res.data === 0) alert(`Not exists employee with id:${id}`)
            // this.props.history.goBack()
            this.setState({ redirect: true })
        }).catch(err => console.log(err))
    }
    render() {
        if(this.state.redirect) 
            return <Redirect to="/" />
        return(<div></div>)
    }
}

export default Delete