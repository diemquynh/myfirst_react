import React from 'react';
import axios from 'axios';
import { Redirect } from "react-router-dom";

class Update extends React.Component{
    constructor(props) {
        super(props);
        const id = this.props.match.params.id;
        this.state = { employee: {id:id, name: '', salary: '', age: '', image: ''}, redirect: false};
    }
    componentDidMount() {
        axios.get(`http://localhost:8000/employee/${this.state.employee.id}`).then(res => {
            const employee = res.data.data;
            console.log(res.data)
            console.log(this.props.history)
            if(employee){
                this.setState(prevState => ({
                    employee: {                   // object that we want to update
                        ...prevState.employee,    // keep all other key-value pairs
                        name: employee.name,       // update the value of specific key
                        salary: employee.salary,       
                        age: employee.age      
                    }
                }));
            }
            else{
                document.getElementById('submit').disabled = true
                alert(`Not exists employee: id = ${this.state.employee.id}`)
                this.props.history.push('/')
            }
        })
    }
    myChangeHandler = (event) => {
        let name = event.target.name
        let value = event.target.value
        this.setState(prevState => ({
            employee: {                   // object that we want to update
                ...prevState.employee,    // keep all other key-value pairs
                // name: 'something'       // update the value of specific key
                [name]: value
            }
        }));
    }
    mySubmitHandler = (event) => {
        event.preventDefault();
        const {id:ID, ...other} = this.state.employee
        console.log({...other})
        axios.put(`http://localhost:8000/update/${ID}`, {...other}).then(() => {
            // window.history.back()
            // this.props.history.goBack()
            this.setState({ redirect: true })
        }).catch(err => console.log(err))
    }
    render(){
        let redirect = ''
        if(this.state.redirect)
            redirect = <Redirect to='/' />
        return (
            <div>
                {redirect}
                <h2>Update employees's informations</h2>
                <form id='update' onSubmit={this.mySubmitHandler}>
                    <div>
                        ID:<br/>
                        <input type='text' name='id' value={this.state.employee.id} readOnly/>
                    </div>
                    <div>
                        Fill name:<br/>
                        <input type='text' name='name' value={this.state.employee.name} onChange={this.myChangeHandler}/>
                    </div>
                    <div>
                        Fill salary:<br/>
                        <input type='text' name='salary' value={this.state.employee.salary} onChange={this.myChangeHandler}/>
                    </div>
                    
                    <div>
                        Fill age:<br/>
                        <input type='text' name='age' value={this.state.employee.age} onChange={this.myChangeHandler}/>
                    </div>
                    <input type='submit' id='submit'/>
                </form>
            </div>
        )
    }
}

export default Update