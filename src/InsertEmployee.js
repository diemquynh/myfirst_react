import React from 'react';
import axios from 'axios';
import { Redirect } from "react-router-dom";

class Insert extends React.Component{
    constructor(props) {
        super(props);
        this.state = { employee: {name: '', salary: '', age: '', image: ''}, redirect: false};
    }
    myChangeHandler = (event) => {
        let name = event.target.name
        let value = event.target.value
        this.setState(prevState => ({
            employee: {                   // object that we want to update
                ...prevState.employee,    // keep all other key-value pairs
                // name: 'something'       // update the value of specific key
                [name]: value
            }
        }));
    }
    mySubmitHandler = (event) => {
        event.preventDefault();
        // const employee = {name:this.state.name, salary:this.state.salary, age:this.state.age, image:this.state.image}
        axios.post(`http://localhost:8000/create`, this.state.employee).then(() => {
            // this.props.history.goBack()
            this.setState({ redirect: true })
        }).catch(err => console.log(err))
    }
    render(){
        let redirect = ''
        if(this.state.redirect)
            redirect = <Redirect to='/' />
        return (
            <div>
                {redirect}
                <h2>Insert an employees's informations</h2>
                <form id='insert' onSubmit={this.mySubmitHandler}>
                    <div>
                        Fill name:<br/>
                        <input type='text' name='name' value={this.state.name} onChange={this.myChangeHandler}/>
                    </div>
                    <div>
                        Fill salary:<br/>
                        <input type='text' name='salary' value={this.state.salary} onChange={this.myChangeHandler}/>
                    </div>
                    
                    <div>
                        Fill age:<br/>
                        <input type='text' name='age' value={this.state.age} onChange={this.myChangeHandler}/>
                    </div>
                    <input type='submit'/>
                </form>
            </div>
        )
    }
}

export default Insert