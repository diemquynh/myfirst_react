import React, { useState } from 'react';
import axios from 'axios';
import { Redirect } from "react-router-dom";

function Insert (){
    const [employee, setEmployee] = useState({ name: '', salary: '', age: '', image: '' })
    const [redirect, setRedirect] = useState( false )
    const [error, setError] = useState({ name:<strong>Fill your name</strong>, salary:'', age:'' })

    function myChangeHandler (event) {
        let name = event.target.name
        let value = event.target.value
        let err = ''
        if(name === 'age' || name === 'salary'){
            if(value !== '' && !Number(value)){
                err = <strong>Your {name} must be a number</strong>
                setError(prev => ({...prev, [name]: err}))
            }
            else setError(prev => ({...prev, [name]: ''}))
        }
        else {
            err = <strong>Fill your {name}</strong>
            value === '' ? setError(prev => ({...prev, [name]: err})) : setError(prev => ({...prev, [name]: ''}))
        }
        setEmployee(prevState => ({ ...prevState, [name]: value }));
    }

    function mySubmitHandler (event) {
        event.preventDefault();
        console.log(error)
        if(error.age === '' && error.salary === '' && error.name === '')
            axios.post(`http://localhost:8000/create`, employee).then(() => {
                setRedirect( true )
            }).catch(err => console.log(err))
    }

    return (
        <div>
            {redirect ? <Redirect to='/' /> : '' } 
            {/* redirect && ( <Redirect to='/' /> ) */}
            <h2>Insert an employees's informations</h2>
            <form id='insert' onSubmit={ mySubmitHandler }>
                <div>
                    Fill name:<br/>
                    <input type='text' name='name' value={employee.name} onChange={myChangeHandler}/> {error.name}
                </div>
                <div>
                    Fill salary:<br/>
                    <input type='text' name='salary' value={employee.salary} onChange={myChangeHandler}/> {error.salary}
                </div>
                <div>
                    Fill age:<br/>
                    <input type='text' name='age' value={employee.age} onChange={myChangeHandler}/> {error.age}
                </div>
                <input type='submit'/>
            </form>
        </div>
    )
}

export default Insert