import React from 'react';
import { Link } from "react-router-dom";

export default function Employee (props) {
    let locationUpdate = '/update/' + props.id
    let locationDelete = '/delete/' + props.id
    return(
        <tr>
            <td>{props.id}</td>
            <td>{props.name}</td>
            <td>{props.salary}</td>
            <td>{props.age}</td>
            <td>{props.image}</td>
            <td><Link to = {locationUpdate}>Update</Link></td>
            <td><Link to = {locationDelete}>Delete</Link></td>
        </tr>
    )
}