import React from 'react';

class UncontrollForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = { errorMessage: '' };
    }

    mySubmitHandler = (event) => {
        event.preventDefault();
        // select
        let select = this.mycar;
        let cars = [].filter.call(select.options, o => o.selected).map(o => o.value);
        // radio
        let radio = this.gender
        let gender = [].filter.call(radio.children, o => o.checked).map(o => o.value);
        // checkbox
        let checkbox = this.job
        let job = [].filter.call(checkbox.children, o => o.checked).map(o => o.value);
        console.log(cars, gender, job)
    }
    render(){
        const inputText = {backgroundColor:'yellow', margin: '10px 0'} 
        return (
            <form onSubmit={this.mySubmitHandler}>
                <label>Fill your name:
                    <input type="text" name='username' defaultValue='' ref={username => this.username = username} style={inputText}/>
                </label>
                <br />
                <label>Fill your age:
                    <input type="text" name='age' ref={age => this.age = age} style={inputText}/> {this.state.errorMessage}
                </label>
                <br/>
                <label>Comment:
                    <textarea name='comment' ref={comment => this.comment = comment}/>
                </label>
                <br />
                <label>Fill your car:
                    <select name='mycar' ref={ mycar => this.mycar = mycar } multiple={true} style={{color:'red', padding: '5px'}}>
                        <option value="Ford">Ford</option>
                        <option value="Volvo">Volvo</option>
                        <option value="Fiat">Fiat</option>
                    </select>
                </label>
                <br />
                <label ref={gender => this.gender = gender}> Choose your gender:
                    <input type='radio' name='gender' value='Nam' defaultChecked={true} /> Nam
                    <input type='radio' name='gender' value='Nữ' /> Nữ
                    <input type='radio' name='gender' value='Other' /> Other
                </label>
                <br />
                <label ref={job => this.job = job}>
                    <input type='checkbox' name='job' value='teacher' />Teacher
                    <input type='checkbox' name='job' value='student'/>Student
                    <input type='checkbox' name='job' value='developer'/>Developer
                </label>
                <br />
                <input type='submit'/>
            </form>
        )
    }
}

export default UncontrollForm